from bliss.controllers.motor import CalcController
import numpy as np

class kroll_nrad_CalcController(CalcController):

    def deg2nrad(self, deg):
        return np.deg2rad(deg)*10**9

    def nrad2deg(self, nrad):
        return np.rad2deg(nrad)/10**9

    def calc_from_real(self, positions_dict):
        return {"nrad": self.deg2nrad(positions_dict["deg"])}

    def calc_to_real(self, positions_dict):
        return {"deg": self.nrad2deg(positions_dict["nrad"])}
