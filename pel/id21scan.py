from bliss.scanning.group import Sequence

def scan_piezo(motor, start, stop):
    
    title = "scan piezo/musst"
    scan_info = ScanInfo()
    scan_info.update({
        "title": title,
        "type": "piezo_musst",
        "dim": 1,
    })
    scan_info.add_curve_plot()
    scan_info.set_channel_meta("scans", group="sequence")
    scan_info.set_channel_meta("scan_numbers", group="sequence")        
    scan_info.add_1d_plot(name=title, x="points",y=["ch1", "ch2", "ch3"])

    seq = Sequence(scan_info=scan_info, title=title)
    with self._seq.sequence_context() as scan_seq:
        
        # move to start position
        umvr(motor, start)
        
        # configure musst
        setup_globals.musst.upload_file("musst_daq.mprg")
        
        # start musst
        setup_globals.musst_nano.run()

        # move motor
        umvr(motor, start)
        
        # read musst
        d=setup_globals.musst_nano.get_data(4)
        
        # Create channels
        data = d.transpose()
        nbp = len(data[0])
        seq.add_custom_channel(AcquisitionChannel("time",numpy.float,(nbp,)))
        seq.add_custom_channel(AcquisitionChannel("ch1",numpy.float,(nbp,)))
        seq.add_custom_channel(AcquisitionChannel("ch2",numpy.float,(nbp,)))
        seq.add_custom_channel(AcquisitionChannel("ch3",numpy.float,(nbp,)))
        
        # Emit channels data
        seq.custom_channels["time"].emit(data[0])
        seq.custom_channels["ch1"].emit(data[1])
        seq.custom_channels["ch2"].emit(data[2])
        seq.custom_channels["ch3"].emit(data[3])
