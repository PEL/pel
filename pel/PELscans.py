from bliss.common.scans import ascan
# from bliss.common.plot import *
from bliss.shell.standard import umv
import gevent


def PELscan(mot, p_st, p_en, nbp, t_int, *counters, **kwargs):

    display_help = kwargs.pop("help", False)

    if display_help:
        print(
            "PELscan(mot, start, stop, nbp, int_time, interf1, counter.... nbscan=n, backlash=val, bidir=True/False, sleep_time=t(s)"
        )
        return

    nbs = kwargs.pop("nbs", 1)
    backlash = kwargs.pop("backlash", 0)
    bidir = kwargs.pop("bidir", False)
    backlash_st = kwargs.pop("backlash_sleep", 0.0)

    stab_time = kwargs.pop("sleep_time", 0.0)
    return_scan = kwargs.get("return_scan", False)
    kwargs["return_scan"] = True

    for loop in range(nbs):
        if bidir and loop % 2 == 1:
            pos_bckl = p_en + backlash
            pos_start = p_en
            pos_end = p_st
        else:
            pos_bckl = p_st - backlash
            pos_start = p_st
            pos_end = p_en

        print('Move motor "%s" to backlash position %g' % (mot.name, pos_bckl))
        umv(mot, pos_bckl)
        # mot.move(pos_bckl)

        gevent.sleep(backlash_st)

        print('Move motor "%s" to 1st scan position %g' % (mot.name, pos_start))
        umv(mot, pos_start)
        # mot.move(pos_start)

        print("Waiting time for 1st point %d (s)" % (t_int,))
        gevent.sleep(t_int)

        print("Scan #%d (/%d) from %g to %g\n\n" % (loop + 1, nbs, pos_start, pos_end))
        scan = ascan(mot, pos_start, pos_end, nbp, t_int, *counters, stab_time=stab_time, **kwargs)

        # data = scan.get_data()

        # plot motor pos vs. counters by default
        # do not plot interferometer since it doesn't give a useful result
        # counters_to_plot = [
        #    name
        #    for name in data.dtype.fields
        #    if name not in ("timestamp", mot.name) and not name.startswith("interf")
        # ]

        # Plotting de-activated by CG 6 aug. 2018... causes crash with flint :(
        #        x = data[mot.name]
        #        y = data[counters_to_plot]
        #        plot(y, x=x)
