# import random
import os
import time
import logging
import smtplib
from datetime import datetime
from email.mime.text import MIMEText

import gevent
from prompt_toolkit.patch_stdout import patch_stdout

from bliss.config.settings import HashSetting
from bliss.common.plot import plot

# from bliss.shell.standard import pprint

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)


COLORS_PRIMARY = ["blue", "cyan", "green"]
COLORS_SECONDARY = ["red", "orange", "yellow"]


def subtract_arrays(array1, array2):
    return [xi - yi for xi, yi in zip(array1, array2)]


class FastJackPlot:
    _history_size = 100
    _plot = False

    def __init__(self, fjs):
        self._fjs = fjs
        self._cycle_history = {"cycle": []}
        self._mim_history = {}

        for fj in self._fjs:
            self._cycle_history[f"{fj.name}:position"] = []
            self._cycle_history[f"{fj.name}:encoder"] = []

    def start_plot(self):
        self.cycle_fig = plot(
            data=None, name="FJT_Cycles", closeable=False, selected=True
        )

        self.cycle_fig.submit("setGraphXLabel", "Cycle")
        self.cycle_fig.submit(
            "setGraphYLabel",
            "Position",
        )
        self.cycle_fig.submit(
            "setGraphYLabel",
            "Error",
            axis="right",
        )
        self.cycle_fig.submit("setGraphGrid", which=True)

        self.cycle_fig.submit(
            "setDataMargins",
            xMinMargin=0.0,
            xMaxMargin=0.0,
            yMinMargin=0.1,
            yMaxMargin=0.1,
        )

        self.mim_fig = plot(data=None, name="FJT_MIM", closeable=False, selected=False)

        self.mim_fig.submit("setGraphXLabel", "Step")
        self.mim_fig.submit(
            "setGraphYLabel",
            "Position",
        )
        self.mim_fig.submit(
            "setGraphYLabel",
            "Encoder",
            axis="right",
        )

    def is_cycle_plot_active(self):
        try:
            return self.cycle_fig._flint.get_plot_name(self.cycle_fig.plot_id)
        except Exception:
            return False

    def is_mim_plot_active(self):
        try:
            return self.mim_fig._flint.get_plot_name(self.mim_fig.plot_id)
        except Exception:
            return False

    def add_cycle(self, cycle_data):
        for pos in [cycle_data[0]]:
            self._cycle_history["cycle"].append(pos[0])

            for i, fj in enumerate(self._fjs):
                self._cycle_history[f"{fj.name}:position"].append(pos[i * 3 + 5])
                self._cycle_history[f"{fj.name}:encoder"].append(pos[i * 3 + 6])

        for data in self._cycle_history.values():
            dx = len(data) - self._history_size
            if dx > 0:
                for i in range(dx):
                    data.pop(0)

        self.plot_cycles()

    def plot_cycles(self):
        if not (self.is_cycle_plot_active() and self._plot):
            return

        self.cycle_fig.submit("setAutoReplot", False)

        self.cycle_fig.add_data(self._cycle_history.get("cycle", []), field="cycle")
        for i, fj in enumerate(self._fjs):
            self.cycle_fig.add_data(
                self._cycle_history.get(f"{fj.name}:position", []),
                field=f"{fj.name}:position",
            )
            self.cycle_fig.add_data(
                subtract_arrays(
                    self._cycle_history.get(f"{fj.name}:encoder", []),
                    self._cycle_history.get(f"{fj.name}:position", []),
                ),
                field=f"{fj.name}:error",
            )

            self.cycle_fig.select_data(
                "cycle",
                f"{fj.name}:position",
                color=COLORS_PRIMARY[i % len(COLORS_PRIMARY)],
                linestyle="-",
            )
            self.cycle_fig.select_data(
                "cycle",
                f"{fj.name}:error",
                color=COLORS_SECONDARY[i % len(COLORS_SECONDARY)],
                linestyle="-",
                yaxis="right",
            )

        self.cycle_fig.submit("setAutoReplot", True)

    def add_mim(self, mim_data):
        mim_plot_data = {"step": []}

        for fj in self._fjs:
            mim_plot_data[f"{fj.name}:position"] = []
            mim_plot_data[f"{fj.name}:encoder"] = []

        first_position = {}
        first_encoder = {}
        for i, fj in enumerate(self._fjs):
            first_position[fj.name] = mim_data[0][i * 3 + 5]
            first_encoder[fj.name] = mim_data[0][i * 3 + 6]

        for step in mim_data:
            mim_plot_data["step"].append(step[0] + step[1] / 10.0)

            for i, fj in enumerate(self._fjs):
                mim_plot_data[f"{fj.name}:position"].append(
                    step[i * 3 + 5] - first_position[fj.name]
                )
                mim_plot_data[f"{fj.name}:encoder"].append(
                    step[i * 3 + 6] - first_encoder[fj.name]
                )

        self._mim_history = mim_plot_data

        self.plot_mim()

    def plot_mim(self):
        if not (self.is_mim_plot_active() and self._plot):
            return

        self.mim_fig.submit("setAutoReplot", False)

        self.mim_fig.add_data(self._mim_history.get("step", []), field="step")
        for i, fj in enumerate(self._fjs):
            self.mim_fig.add_data(
                self._mim_history.get(f"{fj.name}:position", []),
                field=f"{fj.name}:position",
            )
            self.mim_fig.add_data(
                self._mim_history.get(f"{fj.name}:encoder", []),
                field=f"{fj.name}:encoder",
            )

            self.mim_fig.select_data(
                "step",
                f"{fj.name}:position",
                color=COLORS_PRIMARY[i % len(COLORS_PRIMARY)],
                linestyle="-",
            )
            self.mim_fig.select_data(
                "step",
                f"{fj.name}:encoder",
                color=COLORS_SECONDARY[i % len(COLORS_SECONDARY)],
                linestyle="-",
                yaxis="right",
            )

        self.mim_fig.submit("setAutoReplot", True)

    @property
    def plot(self):
        return self._plot

    @plot.setter
    def plot(self, value):
        self._plot = value

        if value:
            try:
                self.start_plot()
                self.plot_cycles()
                self.plot_mim()
            except Exception:
                logger.exception("Cant start flint")


class FastJackStorage:
    _root = "fjtest"

    _cycle = 1
    _last_cycle = 0

    def __init__(self, root_directory):
        logger.info("Ensuring directories exist")
        for ty in ["cycle", "mim"]:
            type_root = os.path.join(self._root, ty)

            if not os.path.exists(type_root):
                logger.info(f"Creating {type_root}")
                os.makedirs(type_root)

    def add_cycle(self, cycle_data):
        now = datetime.now()
        cycle_file = os.path.join(
            self._root, "cycle", f"{now.strftime('%Y-%m-%d')}.asc"
        )

        if not os.path.exists(cycle_file):
            with open(cycle_file, "w") as cf:
                cf.write(
                    "# "
                    + "\t".join(
                        [
                            "cycle",
                            "time",
                            "epoch",
                            "position",
                            "jack",
                            "position",
                            "encoder",
                            "temperature",
                        ]
                    )
                    + "\n"
                )

        formatted_data = []
        with open(cycle_file, "a") as cf:
            for pos in cycle_data:
                cycle_time, position, fjs = pos

                formatted = [
                    self.cycle,
                    datetime.fromtimestamp(cycle_time).strftime("%Y-%m-%d %H:%M:%S"),
                    cycle_time,
                    position,
                ] + [item for fj in fjs for item in fj]

                cf.write(
                    "\t".join(
                        map(
                            str,
                            formatted,
                        )
                    )
                    + "\n"
                )
                formatted_data.append(formatted)

        # self.increment_cycle()

        return formatted_data

    def increment_cycle(self):
        self.cycle = self.cycle + 1

    def add_mim(self, mim_data):
        now = datetime.now()
        mim_file = os.path.join(
            self._root, "mim", f"{now.strftime('%Y-%m-%d_%H%M')}.asc"
        )

        if not os.path.exists(mim_file):
            with open(mim_file, "w") as cf:
                cf.write(f"# cycle {self._cycle}\n")
                cf.write(
                    "# "
                    + "\t".join(
                        [
                            "step",
                            "sample",
                            "time",
                            "epoch",
                            "jack",
                            "position",
                            "encoder",
                        ]
                    )
                    + "\n"
                )

        formatted_data = []
        with open(mim_file, "a") as cf:
            for pos in mim_data:
                cycle_time, step, sample, fjs = pos

                formatted = [
                    step,
                    sample,
                    datetime.fromtimestamp(cycle_time).strftime("%Y-%m-%d %H:%M:%S"),
                    cycle_time,
                ] + [item for fj in fjs for item in fj]

                cf.write(
                    "\t".join(
                        map(
                            str,
                            formatted,
                        )
                    )
                    + "\n"
                )
                formatted_data.append(formatted)

        return formatted_data

    def restore(self):
        cycle_file = os.path.join(self._root, "cycle_state.asc")
        if os.path.exists(cycle_file):
            with open(cycle_file) as cf:
                cycle, time = cf.readline().split(", ")
                self._cycle = int(cycle)
                self._last_cycle = time

                logger.info(
                    f"Restored from cycle {self._cycle}, last cycle was at {self._last_cycle}"
                )
        else:
            logger.warning("No cycle file found, starting at cycle 1")

    @property
    def cycle(self):
        return self._cycle

    @cycle.setter
    def cycle(self, cycle):
        cycle_file = os.path.join(self._root, "cycle_state.asc")
        now = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        with open(cycle_file, "w") as cf:
            cf.write(f"{cycle}, {now}\n")

        self._cycle = cycle
        self._last_cycle = now


class FastJack:
    _last_temp = 0
    _last_step = 1

    def __init__(
        self,
        motor,
        attocube,
        attocube_channel,
        wago_controller,
        temperature_id,
        simulation=False,
        attocube_scale=1,
        attocube_offset=0,
    ):
        self._motor = motor
        self._attocube = attocube
        self._attocube_channel = attocube_channel
        self._attocube_scale = attocube_scale if attocube_scale else 1
        self._attocube_offset = attocube_offset if attocube_offset else 0
        self._wago_controller = wago_controller
        self._temperature_id = temperature_id
        self._simulation = simulation

    @property
    def name(self):
        return self._motor.name

    @property
    def position(self):
        return self._motor.position

    @property
    def motor(self):
        return self._motor

    @property
    def encoder(self):
        if self._simulation:
            import random

            return random.random() * 1000
        measurements = self._attocube.get_measurements()
        return (
            measurements[self._attocube_channel] * self._attocube_scale
            + self._attocube_offset
        )

    @property
    def temperature(self):
        # if self._last_temp > 3:
        #     self._last_step = -1

        # if self._last_temp < 1:
        #     self._last_step = 1

        # self._last_temp += self._last_step
        if self._simulation:
            import random

            return 20 + random.random()  # + self._last_temp
        return self._wago_controller.get(self._temperature_id)


class FastJackTest:
    """
    plugin: bliss
    package: pel.FastJackTest
    class: FastJackTest
    name: fjtest
    config:
        wago: $wago
        motors:
          - object: $m0
            attocube: $atto1
            attocube_channel: 0
            attocube_scale: 1
            attocube_offset: 0
            temperature: tc1
          - object: $m1
            attocube: $atto1
            attocube_channel: 1
            temperature: tc2
          - object: $m2
            attocube: $atto3
            temperature: tc3
    """

    _running = True

    def __init__(self, name, config_tree):
        self._config = config_tree.get("config")
        self._simulation = self._config.get("simulation", False)

        self._fjs = [
            FastJack(
                fj["object"],
                fj["attocube"],
                fj["attocube_channel"],
                self._config["wago"],
                fj["temperature"],
                simulation=self._simulation,
                attocube_scale=fj.get("attocube_scale"),
                attocube_offset=fj.get("attocube_offset"),
            )
            for fj in self._config["motors"]
        ]

        self._settings = HashSetting(
            f"fj_{name}",
            default_values={
                "saving_root": "fjtest",
                "cycle_pause": 0.05,
                "cycle_pause_measure": 0.05,
                "cycle_start_pos": 0,
                "cycle_pause_pos": 1,
                "cycle_backlash": 0,
                "meas_interval": 2,
                "mim_interval": 10,
                "mim_steps": 10,
                "mim_samples": 10,
                "mim_step_size": 0.1,
                "mim_pause": 1,
                "mim_sample_pause": 0.2,
                "grease_interval": 20,
                "grease_pos": 0.1,
                "grease_neg": 0.1,
                "temperature_threshold": 23,
                "temperature_monitor": True,
                "plot": False,
            },
        )

        self._state = FastJackStorage(self._settings["saving_root"])
        self._state.restore()

        self._plot = FastJackPlot(self._fjs)

        if self._settings["plot"]:
            self._plot.plot = True

        self._temperature_thread = gevent.spawn(self._check_temperature)

    def reload(self):
        self._state.restore()

    def __info__(self):
        fjtest_info = "Fast Jack Test Control:\n"
        fjtest_info += f"  Saving Root: {self._settings['saving_root']}\tPlotting:  {self._settings['plot']}\n"
        fjtest_info += f"  Current Cycle: {self._state.cycle}\n  Last Cycle Time: {self._state._last_cycle}\n\n"
        fjtest_info += f"  Cycle Settings\n    Start Position: {self._settings['cycle_start_pos']}\tPause Position: {self._settings['cycle_pause_pos']}\tPause Time: {self._settings['cycle_pause']}s\n"
        fjtest_info += f"    Measure Interval: {self._settings['meas_interval']}\tMeasure Pause: {self._settings['cycle_pause_measure']}s\tBacklash: {self._settings['cycle_backlash']}\n\n"
        fjtest_info += f"  MIM Settings\n    Interval: {self._settings['mim_interval']} iterations\tSteps: {self._settings['mim_steps']}\n"
        fjtest_info += f"    Step Size: {self._settings['mim_step_size']}\tPause Time: {self._settings['mim_pause']} s\n"
        fjtest_info += f"    Samples: {self._settings['mim_samples']}\tSample Time: {self._settings['mim_sample_pause']} s\n\n"
        fjtest_info += f"  Grease Motion\n    Interval: {self._settings['grease_interval']} iterations\tNegative: {self._settings['grease_neg']}\tPostitive: {self._settings['grease_pos']}\n\n"
        fjtest_info += f"  Temperature Monitor\n    Running: {self._settings['temperature_monitor']}\tOvertemperature: {self._settings['temperature_threshold']} c\n\n"

        fjtest_info += (
            "  Motors:\n    Name\tPosition\tEncoder\tTemp\tVelocity\tAcc Time\n"
        )
        for fj in self._fjs:
            temp = fj.temperature
            if temp > self._settings["temperature_threshold"]:
                temp = f"{temp} [Over]"

            fjtest_info += f"    {fj.name}\t{fj.position:.4f}\t{fj.encoder:.4f}\t{temp}\t{fj.motor.velocity}\t{fj.motor.acctime}\n"

        return fjtest_info

    def cycle(self):
        for _, position in enumerate(
            [
                self._settings["cycle_start_pos"],
                self._settings["cycle_pause_pos"],
            ]
        ):
            for fj in self._fjs:
                fj.motor.move(position, wait=False)

            for fj in self._fjs:
                fj.motor.wait_move()

        time.sleep(self._settings["cycle_pause"])

    def cycle_measure(self):
        cycle_data = []
        for posid, position in enumerate(
            [
                self._settings["cycle_start_pos"],
                self._settings["cycle_pause_pos"],
            ]
        ):
            if posid == 0 and self._settings["cycle_backlash"] is not None:
                for fj in self._fjs:
                    fj.motor.move(
                        position - self._settings["cycle_backlash"], wait=False
                    )

                for fj in self._fjs:
                    fj.motor.wait_move()

            for fj in self._fjs:
                fj.motor.move(position, wait=False)

            for fj in self._fjs:
                fj.motor.wait_move()

            time.sleep(self._settings["cycle_pause"])

            fjs = []
            for fj in self._fjs:
                fjs.append((fj.name, fj.position, fj.encoder))

            for fj in self._fjs:
                fjs.append((fj.temperature,))

            cycle_data.append((time.time(), position, fjs))

            time.sleep(self._settings["cycle_pause_measure"])

        return cycle_data

    def _mim_sample(self, step, mim_data):
        for sample in range(self._settings["mim_samples"]):
            fjs = []
            for fj in self._fjs:
                fjs.append((fj.name, fj.position, fj.encoder))

            mim_data.append((time.time(), step, sample, fjs))

            time.sleep(self._settings["mim_sample_pause"])

    def mim(self):
        mim_data = []

        time.sleep(self._settings["mim_pause"])
        self._mim_sample(0, mim_data)

        for step in range(self._settings["mim_steps"]):
            for fj in self._fjs:
                fj.motor.rmove(self._settings["mim_step_size"], wait=False)

            for fj in self._fjs:
                fj.motor.wait_move()

            time.sleep(self._settings["mim_pause"])
            self._mim_sample(step + 1, mim_data)

        return mim_data

    def grease_motion(self):
        for _, position in enumerate(
            [
                0 - self._settings["grease_neg"],
                1 + self._settings["grease_pos"],
            ]
        ):
            for fj in self._fjs:
                fj.motor.move(position, wait=False)

            for fj in self._fjs:
                fj.motor.wait_move()

    def _check_temperature(self):
        while True:
            if self._settings["temperature_monitor"]:
                # logger.info("Checking temperatures")

                over_temp = False
                for fj in self._fjs:
                    # logger.info(f"{fj.motor.name} temperature {fj.temperature}")
                    if fj.temperature > self._settings["temperature_threshold"]:
                        over_temp = True

                        if self._running:
                            self._running = False
                            message = f"  Motor over temperature {fj.motor.name} currently at {fj.temperature:.2f}c, threshold {self._settings['temperature_threshold']}c"
                            with patch_stdout():
                                print(f"\n\u001b[31m{message}\u001b[0m")
                            self._send_notification(message)

                if not over_temp and not self._running:
                    with patch_stdout():
                        print("\n\u001b[32m  Temperatures below threshold\u001b[0m")
                    self._running = True

                time.sleep(10)

            else:
                time.sleep(1)

    def run(self):
        first_iteration = True
        while True:
            try:
                if self._running:
                    if (
                        self._state.cycle % self._settings["meas_interval"] == 0
                        or first_iteration
                    ):
                        formatted = self._state.add_cycle(self.cycle_measure())
                        print(f"Cycle {self._state.cycle} measure finished")
                        self._plot.add_cycle(formatted)
                    else:
                        self.cycle()
                        print(f"Cycle {self._state.cycle} finished")

                    if (
                        self._state.cycle % self._settings["mim_interval"] == 0
                        or first_iteration
                    ):
                        formatted_mim = self._state.add_mim(self.mim())
                        print(f"MIM finished")
                        self._plot.add_mim(formatted_mim)

                    if self._state.cycle % self._settings["grease_interval"] == 0:
                        self.grease_motion()
                        print(f"Grease motion finished")

                    if first_iteration:
                        first_iteration = False

                    self._state.increment_cycle()
                else:
                    print("Waiting for temperatures to be below threshold")
                    time.sleep(5)

            except KeyboardInterrupt:
                print("Stopping test")
                for fj in self._fjs:
                    fj.motor.stop()
                    print(f"{fj.motor.name} stopped")

                print(f"Moving all jacks to start position: {self._settings['cycle_start_pos']}")
                for fj in self._fjs:
                    fj.motor.move(self._settings["cycle_start_pos"])

                for fj in self._fjs:
                    fj.motor.wait_move()

                return

            except Exception as e:
                print(f"Exception in fast jack test, stopping")
                self._send_notification(f"Fast jack test stopped\n{str(e)}")
                raise

    def _send_notification(self, reason):
        if not (
            self._config.get("smtp_server")
            and self._config.get("username")
            and self._config.get("password")
            and self._config.get("notify")
        ):
            logger.warning("Email not configured, not sending notification")
            return

        serv = smtplib.SMTP_SSL(self._config["smtp_server"])
        serv.login(self._config["username"], self._config["password"])

        msg = MIMEText(reason)
        msg["Subject"] = "MEL Bliss Notification"
        msg["From"] = "blissadm@esrf.fr"
        msg["To"] = self._config["notify"]

        serv.sendmail(msg["From"], [msg["To"]], msg.as_string())

    @property
    def temperature_threshold(self):
        return self._settings["temperature_threshold"]

    @temperature_threshold.setter
    def temperature_threshold(self, value):
        self._settings["temperature_threshold"] = value

    @property
    def temperature_monitor(self):
        return self._settings["temperature_monitor"]

    @temperature_monitor.setter
    def temperature_monitor(self, value):
        self._settings["temperature_monitor"] = value

    @property
    def saving_root(self):
        return self._settings["saving_root"]

    @saving_root.setter
    def saving_root(self, value):
        self._settings["saving_root"] = value

    @property
    def cycle_start_pos(self):
        return self._settings["cycle_start_pos"]

    @cycle_start_pos.setter
    def cycle_start_pos(self, value):
        self._settings["cycle_start_pos"] = value

    @property
    def cycle_pause_pos(self):
        return self._settings["cycle_pause_pos"]

    @cycle_pause_pos.setter
    def cycle_pause_pos(self, value):
        self._settings["cycle_pause_pos"] = value

    @property
    def cycle_pause(self):
        return self._settings["cycle_pause"]

    @cycle_pause.setter
    def cycle_pause(self, value):
        self._settings["cycle_pause"] = value

    @property
    def cycle_pause_measure(self):
        return self._settings["cycle_pause_measure"]

    @cycle_pause_measure.setter
    def cycle_pause_measure(self, value):
        self._settings["cycle_pause_measure"] = value

    @property
    def cycle_backlash(self):
        return self._settings["cycle_backlash"]

    @cycle_backlash.setter
    def cycle_backlash(self, value):
        self._settings["cycle_backlash"] = value

    @property
    def meas_interval(self):
        return self._settings["meas_interval"]

    @meas_interval.setter
    def meas_interval(self, value):
        self._settings["meas_interval"] = value

    @property
    def mim_interval(self):
        return self._settings["mim_interval"]

    @mim_interval.setter
    def mim_interval(self, value):
        self._settings["mim_interval"] = value

    @property
    def mim_steps(self):
        return self._settings["mim_steps"]

    @mim_steps.setter
    def mim_steps(self, value):
        self._settings["mim_steps"] = value

    @property
    def mim_step_size(self):
        return self._settings["mim_step_size"]

    @mim_step_size.setter
    def mim_step_size(self, value):
        self._settings["mim_step_size"] = value

    @property
    def mim_pause(self):
        return self._settings["mim_pause"]

    @mim_pause.setter
    def mim_pause(self, value):
        self._settings["mim_pause"] = value

    @property
    def mim_sample_pause(self):
        return self._settings["mim_sample_pause"]

    @mim_sample_pause.setter
    def mim_sample_pause(self, value):
        self._settings["mim_sample_pause"] = value

    @property
    def grease_interval(self):
        return self._settings["grease_interval"]

    @grease_interval.setter
    def grease_interval(self, value):
        self._settings["grease_interval"] = value

    @property
    def grease_neg(self):
        return self._settings["grease_neg"]

    @grease_neg.setter
    def grease_neg(self, value):
        self._settings["grease_neg"] = value

    @property
    def grease_pos(self):
        return self._settings["grease_pos"]

    @grease_pos.setter
    def grease_pos(self, value):
        self._settings["grease_pos"] = value

    @property
    def plot(self):
        return self._settings["plot"]

    @plot.setter
    def plot(self, value):
        self._settings["plot"] = value
        self._plot.plot = value
