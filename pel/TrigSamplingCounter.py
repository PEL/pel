# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2020 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

"""
YAML_ configuration example:

.. code-block:: yaml
    - class: TrigSamplingCounterController
      package: pel.TrigSamplingCounter
      name: interf
      high_time: 0.3
      period: 0.6
      nb_pulses: 10
      trigger_device:
          type: opiom
          name: $OPIOM_NAME
          counter_number: 1     # CNT 1
"""
import gevent

from bliss.controllers.counter import SamplingCounterController
from bliss.common.counter import SamplingMode, SamplingCounter
from bliss.config.settings import HashSetting


class TrigSamplingCounterController(SamplingCounterController):
    def __init__(self, name, config):
        super().__init__(name)
        self.config = config
        trig_config = config["trigger_device"]

        self._opiom_card = trig_config.get("name")
        self._opiom_counter = trig_config.get("counter_number", 1)  # default 1
        self._clock = config.get("clock", 2000000)

        self._counter_config = HashSetting(
            "%s:counter_config" % self.name,
            default_values={
                "high_time": config.get("high_time", 1),
                "period": config.get("period", 1),
                "nb_pulses": config.get("nb_pulses", 1),
            },
        )

        counter_name = f"out{self._opiom_counter}"
        self.create_counter(SamplingCounter, counter_name, mode=SamplingMode.SINGLE)

    @property
    def clock(self):
        return self._clock

    @property
    def high_time(self):
        """
        High time in second for the pulse
        """
        return self._counter_config["high_time"]

    @high_time.setter
    def high_time(self, value):
        self._counter_config["high_time"] = value

    @property
    def period(self):
        """
        period in second for the pulse (high + low level)
        """
        return self._counter_config["period"]

    @period.setter
    def period(self, value):
        self._counter_config["period"] = value

    @property
    def nb_pulses(self):
        """
        Nb pulses generated when trigged
        """
        return self._counter_config["nb_pulses"]

    @nb_pulses.setter
    def nb_pulses(self, value):
        self._counter_config["nb_pulses"] = value

    def read(self, counter):
        opiom = self._opiom_card
        counter = self._opiom_counter
        high_time = self._counter_config["high_time"] * self._clock
        low_time = (
            self._counter_config["period"] - self._counter_config["high_time"]
        ) * self._clock
        opiom.comm("CNT %d STOP" % counter)
        opiom.comm("CNT %d RESET" % counter)
        opiom.comm(
            "CNT %d CLK2 PULSE %d %d %d"
            % (counter, high_time, low_time, self._counter_config["nb_pulses"])
        )

        opiom.comm("CNT %d START" % counter)

        comm = "?SCNT %d" % counter
        state = opiom.comm(comm)

        while state == "RUN":
            state = opiom.comm(comm)
            gevent.sleep(0.1)

        return self._counter_config["nb_pulses"]

    def __info__(self):
        params = " ".join(["%s=%s" % (k, v) for k, v in self._counter_config.items()])
        params += f" | Ch: {self._opiom_counter}"
        return "%s %s: %s" % (self.__class__.__name__, self.name, params)
