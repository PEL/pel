class Gantry:
    """
    plugin: bliss
    package: pel.Gantry
    class: Gantry
    name: gantry
    config:
        wago: $wago
    """
    def __init__(self, name, config_tree):
        self._config = config_tree.get("config")
        self._wago = self._config["wago"]

    def __info__(self):
        info_str = "ETEL Gantry\n"

        pump_state = "Running" if self._wago.get("relay3") else "Stopped"
        info_str += f"  Pump: {pump_state}\n\n  Interlocks:\n"

        relays = {
            "relay1": "ETEL",
            "relay2": "Vacuum",
            "relay3": "Pressure",
        }
        for wago_output, relay_name in relays.items():
            state = "Ok" if self._wago.get(wago_output) else "Tripped"
            info_str += f"    {relay_name}: {state}\n"

        return info_str

    def start_pump(self):
        self._wago.set("relay4", 1)

    def stop_pump(self):
        self._wago.set("relay4", 0)
