===========
PEL project
===========

[![build status](https://gitlab.esrf.fr/PEL/pel/badges/master/build.svg)](http://PEL.gitlab-pages.esrf.fr/PEL)
[![coverage report](https://gitlab.esrf.fr/PEL/pel/badges/master/coverage.svg)](http://PEL.gitlab-pages.esrf.fr/pel/htmlcov)

PEL software & configuration

Latest documentation from master can be found [here](http://PEL.gitlab-pages.esrf.fr/pel)
